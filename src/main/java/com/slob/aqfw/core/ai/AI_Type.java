package com.slob.aqfw.core.ai;

public enum AI_Type {
    BASE_FIGHTER_AI("FIGHTER_BASE");

    private final String name;

    AI_Type(String name) {
        this.name = name;
    }

    public final String getName() {
        return name;
    }
}
