package com.slob.aqfw.core.effect;

public enum EffectAdditionalValueType {
    SIDE,
    DURATION
}
