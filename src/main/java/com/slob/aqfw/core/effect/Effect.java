package com.slob.aqfw.core.effect;

import java.util.HashMap;
import java.util.Map;

public class Effect {
    private final EffectType effectType;
    private final int value;
    private final Map<EffectAdditionalValueType, Object> additionalValues;

    public Effect(EffectType effectType, int value) {
        this.effectType = effectType;
        this.value = value;
        this.additionalValues = new HashMap<>();
    }

    public EffectType getEffectType() {
        return effectType;
    }

    public int getValue() {
        return value;
    }

    public Map<EffectAdditionalValueType, Object> getAdditionalValues() {
        return additionalValues;
    }
}
