package com.slob.aqfw.core.agony;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;

public abstract class Agony {
    protected final Injector injector;

    protected Agony(Injector injector) {
        this.injector = injector;
    }

    public abstract void perform(Cell cell);
}
