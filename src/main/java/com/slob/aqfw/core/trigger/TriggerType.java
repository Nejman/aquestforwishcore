package com.slob.aqfw.core.trigger;

public enum TriggerType {
    KILL_ALL("KILL_ALL"),
    KILL_SPECIFIC("KILL_SPECIFIC"),
    GO_TO_ZONE_ALL("GO_TO_ZONE_ALL"),
    GO_TO_ZONE_ANY("GO_TO_ZONE_ANY"),
    SURVIVE_ANY("SURVIVE_ANY"),
    LEVEL_START("LEVEL_START"),
    SURVIVE_SPECIFIC("SURVIVE_SPECIFIC");

    private final String code;

    TriggerType(String code) {
        this.code = code;
    }

    public static TriggerType getTypeByCode(String code) {
        for (TriggerType triggerType : TriggerType.values()) {
            if (triggerType.code.equals(code)) {
                return triggerType;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
