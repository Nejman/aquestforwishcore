package com.slob.aqfw.core.trigger;

public enum TriggerResultType {
    WIN("WIN"),
    LOSE("LOSE"),
    ADD_TRIGGERS("ADD_TRIGGERS"),
    SPAWN_CHARACTERS("SPAWN_CHARACTERS"),
    DIALOG("DIALOG"),
    ADD_AURAS("ADD_AURAS");

    private final String code;

    TriggerResultType(String code) {
        this.code = code;
    }

    public static TriggerResultType getResultTypeByCode(String code) {
        for (TriggerResultType triggerType : TriggerResultType.values()) {
            if (triggerType.code.equals(code)) {
                return triggerType;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
