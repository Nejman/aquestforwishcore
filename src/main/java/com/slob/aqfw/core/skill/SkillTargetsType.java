package com.slob.aqfw.core.skill;

public enum SkillTargetsType {
    LINE("LINE"),
    ANYWHERE("ANYWHERE"),
    SELF("SELF");

    private final String code;

    SkillTargetsType(String code) {
        this.code = code;
    }

    public static SkillTargetsType getByCode(String code) {
        for (SkillTargetsType targetsType : SkillTargetsType.values()) {
            if (targetsType.code.equals(code)) {
                return targetsType;
            }
        }
        throw new IllegalArgumentException("Unknown targeting type code: " + code);
    }
}
