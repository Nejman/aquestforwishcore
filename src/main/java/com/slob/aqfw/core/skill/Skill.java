package com.slob.aqfw.core.skill;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.descriptor.SkillDescriptor;
import com.slob.aqfw.core.entities.Entity;

import java.util.List;
import java.util.Map;

public abstract class Skill {
    private final String id;
    private final int range;
    private final ImmutableList<Side> allowedSides;
    private final SkillTargetsType targetsType;
    private final Entity user;
    private final Map<PointType, Integer> requiredPoints;
    private final boolean isPassive;
    private final int delay;
    private final Map<String, String> valuesMap;
    private int currentDelay;

    public Skill(SkillDescriptor descriptor, Entity user) {
        this.id = descriptor.getId();
        this.range = descriptor.getRange();
        this.allowedSides = getSidesFromDescriptor(descriptor);
        this.targetsType = SkillTargetsType.getByCode(descriptor.getTargetType());
        this.user = user;
        this.requiredPoints = getPointsFromDescriptor(descriptor);
        this.isPassive = descriptor.getPassive();
        this.valuesMap = getValuesFromDescriptor(descriptor);
        this.delay = descriptor.getDelay();
        currentDelay = 0;
    }

    public abstract void apply(Cell targetedCell);

    public int getRange() {
        return range;
    }

    public ImmutableList<Side> getAllowedSides() {
        return allowedSides;
    }

    public SkillTargetsType getTargetsType() {
        return targetsType;
    }

    public Entity getUser() {
        return user;
    }

    public List<Cell> modifyTargetedCells(List<Cell> originalCells) {
        return originalCells;
    }

    public boolean isPassive() {
        return isPassive;
    }

    public Map<PointType, Integer> getRequiredPoints() {
        return ImmutableMap.copyOf(requiredPoints);
    }


    private Map<String, String> getValuesFromDescriptor(SkillDescriptor descriptor) {
        ImmutableMap.Builder<String, String> result = ImmutableMap.builder();
        if (descriptor.getValues() != null) {
            for (SkillDescriptor.Value value : descriptor.getValues()) {
                result.put(value.name, value.value);
            }
        }
        return result.build();
    }

    private ImmutableList<Side> getSidesFromDescriptor(SkillDescriptor descriptor) {
        ImmutableList.Builder<Side> result = ImmutableList.builder();
        if (descriptor.getSide() != null) {
            for (Integer sideCode : descriptor.getSide()) {
                result.add(Side.getSideForCode(sideCode));
            }
        }
        return result.build();
    }

    private Map<PointType, Integer> getPointsFromDescriptor(SkillDescriptor descriptor) {
        ImmutableMap.Builder<PointType, Integer> result = ImmutableMap.builder();
        if (descriptor.getPoints() != null) {
            for (SkillDescriptor.Point point : descriptor.getPoints()) {
                result.put(PointType.getByCode(point.pointType), point.amount);
            }
        }
        return result.build();
    }

    protected Integer getIntegerValue(SkillValueType type) {
        if (valuesMap.containsKey(type.getCode())) {
            return Integer.valueOf(valuesMap.get(type.getCode()));
        }
        throw new IllegalArgumentException("Value of type: " + type.getCode() + " not present");
    }

    public int getCurrentDelay() {
        return currentDelay;
    }

    public void setCurrentDelay(int currentDelay) {
        this.currentDelay = currentDelay;
    }

    public int getDelay() {
        return delay;
    }

    public String getId() {
        return id;
    }

    public boolean isMovementSkill() {
        return false;
    }
}
