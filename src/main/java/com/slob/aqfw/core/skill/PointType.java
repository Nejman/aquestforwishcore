package com.slob.aqfw.core.skill;

import com.slob.aqfw.core.effect.EffectType;

public enum PointType {
    ACTION(EffectType.ACTION_POINT_CHANGE, "ACTION"),
    MOVEMENT(EffectType.MOVE_POINT_CHANGE, "MOVEMENT"),
    ATTACK(EffectType.ATTACK_POINT_CHANGE, "ATTACK");

    private final EffectType correspondingEffectType;
    private final String code;

    PointType(EffectType correspondingEffectType, String code) {
        this.correspondingEffectType = correspondingEffectType;
        this.code = code;
    }

    public EffectType getCorrespondingEffectType() {
        return correspondingEffectType;
    }

    public static PointType getByCode(String code) {
        for (PointType type : PointType.values()) {
            if (type.code.equals(code)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
