package com.slob.aqfw.core.skill;

public enum SkillValueType {
    POINT_TYPE("POINT_TYPE"),
    CHANGE_VALUE("CHANGE_VALUE"),
    CALLED_CHARACTER("CALLED_CHARACTER("),
    CHANGE_THREAT_VALUE("CHANGE_THREAT_VALUE"),
    DURATION("DURATION"),
    MINIMUM_RANGE("MINIMUM_RANGE"),
    CHANGE_ARMOUR_VALUE("CHANGE_ARMOUR_VALUE"),
    SECONDARY_DAMAGE_VALUE("SECONDARY_DAMAGE_VALUE");

    private final String code;

    SkillValueType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
