package com.slob.aqfw.core.item;

import com.google.inject.Injector;
import com.slob.aqfw.core.cell.Cell;
import com.slob.aqfw.core.cell.Pickable;
import com.slob.aqfw.core.entities.Entity;

import java.util.List;

public abstract class Item implements Pickable {

    @Override
    public void pickUp() {

    }

    public abstract void use(Entity character, Cell targetCell, Injector injector);

    public List<Cell> modifyTargetCells(List<Cell> cells) {
        return cells;
    }

    public boolean isPassive() {
        return false;
    }

    public int getRange() {
        return 1;
    }
}
