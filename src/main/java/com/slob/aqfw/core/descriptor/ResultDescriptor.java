package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

public class ResultDescriptor extends GenericDescriptor {
    @XmlElement(name = "cell")
    private List<CellDescriptor> cell;
    @XmlElement(name = "ids")
    private List<String> ids;
    @XmlElement(name = "resultType")
    private String resultType;

    public ResultDescriptor() {
        this.cell = null;
        this.ids = null;
        this.resultType = null;
    }

    public String getResultType() {
        return resultType;
    }

    public List<CellDescriptor> getCell() {
        return cell;
    }

    public List<String> getIds() {
        return ids;
    }


    @XmlRootElement(name = "results")
    public static class ResultsDescriptors {
        @XmlElement(name = "result")
        private List<ResultDescriptor> resultDescriptors;

        public ResultsDescriptors() {
            resultDescriptors = null;
        }

        public List<ResultDescriptor> getResultDescriptors() {
            return resultDescriptors;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class CellDescriptor {
        @XmlAttribute
        public int x;
        @XmlAttribute
        public int y;
    }
}
