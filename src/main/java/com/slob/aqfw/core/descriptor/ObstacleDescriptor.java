package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "obstacle")
public class ObstacleDescriptor extends GenericDescriptor {
    @XmlElement(name = "maxHealth")
    private int defaultMaxHealth;
    @XmlElement(name = "armour")
    private List<Armour> defaultArmour;
    @XmlElement(name = "alignment")
    private String alignment;
    @XmlElement(name = "threatLevel")
    private int threatLevel;
    @XmlElement(name = "agony")
    private List<String> agonyList;

    public ObstacleDescriptor() {
        this.defaultMaxHealth = 0;
        this.defaultArmour = null;
        this.alignment = null;
        this.threatLevel = 0;
        this.agonyList = null;
    }

    public int getDefaultMaxHealth() {
        return defaultMaxHealth;
    }

    public String getAlignment() {
        return alignment;
    }

    public int getThreatLevel() {
        return threatLevel;
    }

    public List<String> getAgonyList() {
        return agonyList;
    }

    public List<Armour> getDefaultArmour() {
        return defaultArmour;
    }

    @XmlRootElement(name = "obstacles")
    public static class ObstacleDescriptors {
        @XmlElement(name = "obstacle")
        private List<ObstacleDescriptor> obstacleDescriptors;

        public ObstacleDescriptors() {
            obstacleDescriptors = null;
        }

        public List<ObstacleDescriptor> getObstacleDescriptors() {
            return obstacleDescriptors;
        }
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Armour {
        @XmlAttribute
        public Integer side;
        @XmlValue
        public Integer amount;
    }
}
