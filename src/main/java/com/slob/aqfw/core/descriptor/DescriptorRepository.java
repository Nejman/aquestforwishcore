package com.slob.aqfw.core.descriptor;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

@Singleton
public class DescriptorRepository {
    private Map<Class, Map<String, GenericDescriptor>> descriptorMap;

    @Inject
    public DescriptorRepository() {
        descriptorMap = new HashMap<>();
    }

    public void addDescriptor(GenericDescriptor descriptor) {
        if (!descriptorMap.containsKey(descriptor.getClass())) {
            descriptorMap.put(descriptor.getClass(), new HashMap<>());
        }
        descriptorMap.get(descriptor.getClass()).put(descriptor.getId(), descriptor);
    }

    Map<Class, Map<String, GenericDescriptor>> getDescriptorMap() {
        return descriptorMap;
    }
}
