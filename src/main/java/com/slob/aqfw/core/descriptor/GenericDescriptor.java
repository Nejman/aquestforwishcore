package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.XmlElement;

public class GenericDescriptor {
    @XmlElement(name = "id")
    private String id;

    public GenericDescriptor() {
    }

    public String getId() {
        return id;
    }
}
