package com.slob.aqfw.core.descriptor;

import com.google.inject.Inject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class DescriptorReader {
    private final DescriptorRepository descriptorRepository;

    @Inject
    public DescriptorReader(DescriptorRepository descriptorRepository) {
        this.descriptorRepository = descriptorRepository;
        init();
    }

    private void init() {
        CharacterDescriptor.CharacterDescriptors characterDescriptors = (CharacterDescriptor.CharacterDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(CharacterDescriptor.class), CharacterDescriptor.CharacterDescriptors.class);
        putInRepository(characterDescriptors.getCharacterDescriptors());

        SkillDescriptor.SkillDescriptors skillDescriptors = (SkillDescriptor.SkillDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(SkillDescriptor.class), SkillDescriptor.SkillDescriptors.class);
        putInRepository(skillDescriptors.getSkillDescriptors());

        AuraDescriptor.AurasDescriptors aurasDescriptors = (AuraDescriptor.AurasDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(AuraDescriptor.class), AuraDescriptor.AurasDescriptors.class);
        putInRepository(aurasDescriptors.getAuraDescriptors());

        TriggerDescriptor.TriggersDescriptors triggersDescriptors = (TriggerDescriptor.TriggersDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(TriggerDescriptor.class), TriggerDescriptor.TriggersDescriptors.class);
        putInRepository(triggersDescriptors.getTriggerDescriptors());

        ResultDescriptor.ResultsDescriptors resultsDescriptors = (ResultDescriptor.ResultsDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(ResultDescriptor.class), ResultDescriptor.ResultsDescriptors.class);
        putInRepository(resultsDescriptors.getResultDescriptors());

        DialogDescriptor.DialogsDescriptors dialogsDescriptors = (DialogDescriptor.DialogsDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(DialogDescriptor.class), DialogDescriptor.DialogsDescriptors.class);
        putInRepository(dialogsDescriptors.getDialogDescriptors());

        LevelDescriptor.LevelsDescriptors levelsDescriptors = (LevelDescriptor.LevelsDescriptors) getFromFile(DescriptorFilenameHelper.getFilenameForDescriptorClass(LevelDescriptor.class), LevelDescriptor.LevelsDescriptors.class);
        putInRepository(levelsDescriptors.getLevelDescriptors());
    }

    private <T extends GenericDescriptor> void putInRepository(List<T> descriptorList) {
        if (descriptorList != null) {
            for (T descriptor : descriptorList) {
                descriptorRepository.addDescriptor(descriptor);
            }
        }
    }

    public Object getFromFile(String filepath, Class clazz) {
        Object result;
        File file = new File(filepath);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = clazz.cast(jaxbUnmarshaller.unmarshal(file));
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
        return result;
    }


    public <T extends GenericDescriptor> T getById(Class<T> clazz, String id) {
        return clazz.cast(descriptorRepository.getDescriptorMap().get(clazz).get(id));
    }
}
