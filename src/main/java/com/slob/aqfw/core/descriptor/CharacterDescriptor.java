package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "character")
public class CharacterDescriptor extends GenericDescriptor {
    @XmlElement(name = "maxHealth")
    private int defaultMaxHealth;
    @XmlElement(name = "points")
    private List<Point> maxPoints;
    @XmlElement(name = "armour")
    private List<Armour> defaultArmour;
    @XmlElement(name = "alignment")
    private String alignment;
    @XmlElement(name = "defaultSpeed")
    private int defaultSpeed;
    @XmlElement(name = "baseAttackValue")
    private int baseAttackValue;
    @XmlElement(name = "threatLevel")
    private int threatLevel;
    @XmlElement(name = "skills")
    private List<String> skills;
    @XmlElement(name = "agony")
    private List<String> agonyList;
    @XmlElement(name = "aiHandler")
    private String aiHandler;

    public CharacterDescriptor() {
        this.defaultMaxHealth = 0;
        this.maxPoints = null;
        this.defaultArmour = null;
        this.alignment = null;
        this.defaultSpeed = 0;
        this.baseAttackValue = 0;
        this.threatLevel = 0;
        this.skills = null;
        this.agonyList = null;
        this.aiHandler = null;
    }

    public int getDefaultMaxHealth() {
        return defaultMaxHealth;
    }

    public String getAlignment() {
        return alignment;
    }

    public int getDefaultSpeed() {
        return defaultSpeed;
    }

    public int getBaseAttackValue() {
        return baseAttackValue;
    }

    public int getThreatLevel() {
        return threatLevel;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<String> getAgonyList() {
        return agonyList;
    }

    public List<Armour> getDefaultArmour() {
        return defaultArmour;
    }

    public List<Point> getMaxPoints() {
        return maxPoints;
    }

    public String getAiHandler() {
        return aiHandler;
    }

    @XmlRootElement(name = "characters")
    public static class CharacterDescriptors {
        @XmlElement(name = "character")
        private List<CharacterDescriptor> characterDescriptors;

        public CharacterDescriptors() {
            characterDescriptors = null;
        }

        public List<CharacterDescriptor> getCharacterDescriptors() {
            return characterDescriptors;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Point {
        @XmlAttribute
        public String pointType;
        @XmlValue
        public Integer amount;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Armour {
        @XmlAttribute
        public Integer side;
        @XmlValue
        public Integer amount;
    }
}
