package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

public class DictionaryDescriptor extends GenericDescriptor {
    @XmlElement(name = "value")
    private List<Entry> values;

    public DictionaryDescriptor() {
        this.values = null;
    }

    public List<Entry> getValues() {
        return values;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Entry {
        @XmlAttribute
        public String lang;
        @XmlValue
        public String value;
    }

    @XmlRootElement(name = "dictionary")
    public static class DictionaryDescriptors {
        @XmlElement(name = "entry")
        private List<DictionaryDescriptor> dictionaryDescriptor;

        public DictionaryDescriptors() {
            dictionaryDescriptor = null;
        }

        public List<DictionaryDescriptor> getDictionaryDescriptors() {
            return dictionaryDescriptor;
        }
    }
}
