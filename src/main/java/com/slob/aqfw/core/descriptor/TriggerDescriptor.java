package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

public class TriggerDescriptor extends GenericDescriptor {
    @XmlElement(name = "shouldDisplay")
    private Boolean shouldDisplay;
    @XmlElement(name = "cell")
    private List<CellDescriptor> cell;
    @XmlElement(name = "characterId")
    private List<String> characterId;
    @XmlElement(name = "time")
    private int time;
    @XmlElement(name = "triggerType")
    private String triggerType;
    @XmlElement(name = "result")
    private List<String> results;

    public TriggerDescriptor() {
        this.shouldDisplay = null;
        this.cell = null;
        this.characterId = null;
        this.time = 0;
        this.triggerType = null;
        this.results = null;
    }

    public List<String> getResults() {
        return results;
    }

    public String getTriggerType() {
        return triggerType;
    }

    public int getTime() {
        return time;
    }

    public List<String> getCharacterIds() {
        return characterId;
    }

    public List<CellDescriptor> getCell() {
        return cell;
    }

    public Boolean getShouldDisplay() {
        return shouldDisplay;
    }


    @XmlRootElement(name = "triggers")
    public static class TriggersDescriptors {
        @XmlElement(name = "trigger")
        private List<TriggerDescriptor> triggerDescriptors;

        public TriggersDescriptors() {
            triggerDescriptors = null;
        }

        public List<TriggerDescriptor> getTriggerDescriptors() {
            return triggerDescriptors;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class CellDescriptor {
        @XmlAttribute
        public int x;
        @XmlAttribute
        public int y;
    }
}
