package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

public class DialogDescriptor extends GenericDescriptor {
    @XmlElement(name = "statement")
    private List<StatementDescriptor> statements;

    public DialogDescriptor() {
        this.statements = null;
    }

    public List<StatementDescriptor> getstatements() {
        return statements;
    }


    @XmlRootElement(name = "dialogs")
    public static class DialogsDescriptors {
        @XmlElement(name = "dialog")
        private List<DialogDescriptor> dialogDescriptors;

        public DialogsDescriptors() {
            dialogDescriptors = null;
        }

        public List<DialogDescriptor> getDialogDescriptors() {
            return dialogDescriptors;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class StatementDescriptor {
        @XmlElement
        public String character;
        @XmlElement
        public String dictionaryCode;
    }
}
