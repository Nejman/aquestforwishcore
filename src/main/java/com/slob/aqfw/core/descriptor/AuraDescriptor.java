package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

public class AuraDescriptor extends GenericDescriptor {
    @XmlElement(name = "alignment")
    private String alignment;
    @XmlElement(name = "range")
    private int range;
    @XmlElement(name = "duration")
    private int duration;
    @XmlElement(name = "value")
    private int value;

    public AuraDescriptor() {
        this.alignment = null;
        this.range = 0;
        this.duration = 0;
        this.value = 0;
    }

    public int getValue() {
        return value;
    }

    public int getDuration() {
        return duration;
    }

    public int getRange() {
        return range;
    }

    public String getAlignment() {
        return alignment;
    }


    @XmlRootElement(name = "auras")
    public static class AurasDescriptors {
        @XmlElement(name = "aura")
        private List<AuraDescriptor> auraDescriptors;

        public AurasDescriptors() {
            auraDescriptors = null;
        }

        public List<AuraDescriptor> getAuraDescriptors() {
            return auraDescriptors;
        }
    }
}
