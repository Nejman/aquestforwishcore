package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

public class LevelDescriptor extends GenericDescriptor {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "nextLevelId")
    private String nextLevelId;
    @XmlElement(name = "campDialogId")
    private String campDialogId;
    @XmlElement(name = "size")
    private Integer size;
    @XmlElement(name = "entity")
    private List<EntityDescriptor> entities;
    @XmlElement(name = "trigger")
    private List<TriggerDescriptor> triggers;

    public LevelDescriptor() {
        this.name = null;
        this.nextLevelId = null;
        this.size = null;
        this.entities = null;
        this.triggers = null;
        this.campDialogId = null;
    }

    public LevelDescriptor(String name, String nextLevelId, Integer size, List<EntityDescriptor> entities, List<TriggerDescriptor> triggers, String campDialogId) {
        this.name = name;
        this.nextLevelId = nextLevelId;
        this.size = size;
        this.entities = entities;
        this.triggers = triggers;
        this.campDialogId = campDialogId;
    }

    public List<TriggerDescriptor> getTriggers() {
        return triggers;
    }

    public List<EntityDescriptor> getEntities() {
        return entities;
    }

    public Integer getSize() {
        return size;
    }

    public String getNextLevelId() {
        return nextLevelId;
    }

    public String getName() {
        return name;
    }

    public String getCampDialogId() {
        return campDialogId;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    public static class EntityDescriptor {
        @XmlElement(name = "id")
        public String id;
        @XmlElement(name = "uniqueId")
        public String uniqueId;
        @XmlElement(name = "cell")
        public CellDescriptor cell;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class CellDescriptor {
        @XmlAttribute
        public int x;
        @XmlAttribute
        public int y;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class TriggerDescriptor {
        @XmlAttribute
        public String id;
    }

    @XmlRootElement(name = "levels")
    public static class LevelsDescriptors {
        @XmlElement(name = "level")
        private List<LevelDescriptor> levelDescriptors;

        public LevelsDescriptors() {
            levelDescriptors = null;
        }

        public List<LevelDescriptor> getLevelDescriptors() {
            return levelDescriptors;
        }
    }
}
