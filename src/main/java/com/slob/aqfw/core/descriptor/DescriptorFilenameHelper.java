package com.slob.aqfw.core.descriptor;

import com.google.common.collect.ImmutableMap;

public class DescriptorFilenameHelper {
    private static final ImmutableMap<Class, String> FILENAME_MAP;

    static {
        ImmutableMap.Builder<Class, String> FILENAME_MAP_BUILDER = ImmutableMap.builder();
        FILENAME_MAP_BUILDER.put(CharacterDescriptor.class, "characterDescriptors.xml");
        FILENAME_MAP_BUILDER.put(AuraDescriptor.class, "auraDescriptors.xml");
        FILENAME_MAP_BUILDER.put(SkillDescriptor.class, "skillDescriptors.xml");
        FILENAME_MAP_BUILDER.put(DictionaryDescriptor.class, "dictionary.xml");
        FILENAME_MAP_BUILDER.put(TriggerDescriptor.class, "triggerDescriptors.xml");
        FILENAME_MAP_BUILDER.put(ResultDescriptor.class, "resultDescriptors.xml");
        FILENAME_MAP_BUILDER.put(DialogDescriptor.class, "dialogDescriptors.xml");
        FILENAME_MAP_BUILDER.put(LevelDescriptor.class, "levelDescriptors.xml");
        FILENAME_MAP = FILENAME_MAP_BUILDER.build();
    }

    public static String getFilenameForDescriptorClass(Class clazz) {
        return FILENAME_MAP.get(clazz);
    }
}
