package com.slob.aqfw.core.descriptor;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "skill")
public class SkillDescriptor extends GenericDescriptor {
    @XmlElement(name = "points")
    private List<Point> points;
    @XmlElement(name = "targetType")
    private String targetType;
    @XmlElement(name = "side")
    private List<Integer> side;
    @XmlElement(name = "passive")
    private Boolean passive;
    @XmlElement(name = "range")
    private int range;
    @XmlElement(name = "delay")
    private int delay;
    @XmlElement(name = "value")
    private List<Value> values;

    public SkillDescriptor() {
        this.points = null;
        this.targetType = null;
        this.side = null;
        this.passive = null;
        this.range = 0;
        this.delay = 0;
        this.values = null;
    }

    public List<Point> getPoints() {
        return points;
    }

    public List<Integer> getSide() {
        return side;
    }

    public Boolean getPassive() {
        return passive;
    }

    public int getRange() {
        return range;
    }

    public int getDelay() {
        return delay;
    }

    public List<Value> getValues() {
        return values;
    }

    public String getTargetType() {
        return targetType;
    }


    @XmlRootElement(name = "skills")
    public static class SkillDescriptors {
        @XmlElement(name = "skill")
        private List<SkillDescriptor> skillDescriptors;

        public SkillDescriptors() {
            skillDescriptors = null;
        }

        public List<SkillDescriptor> getSkillDescriptors() {
            return skillDescriptors;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Point {
        @XmlAttribute
        public String pointType;
        @XmlValue
        public Integer amount;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    public static class Value {
        @XmlAttribute
        public String name;
        @XmlValue
        public String value;
    }
}
