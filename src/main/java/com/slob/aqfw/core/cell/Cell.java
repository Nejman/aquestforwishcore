package com.slob.aqfw.core.cell;

import com.slob.aqfw.core.entities.Entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Cell {
    private final String id;
    private final Map<Side, Cell> neighbours = new HashMap<>(6);
    private final int x;
    private final int y;
    private final int offset;
    private Entity character;
    private int threat;
    private Pickable pickable;


    public Cell(int x, int y, String id, int offset) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.offset = offset;
        character = null;
        threat = 0;
        pickable = null;
    }

    public String getId() {
        return id;
    }

    public Map<Side, Cell> getNeighbours() {
        return neighbours;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Optional<Entity> getCharacter() {
        return Optional.ofNullable(character);
    }

    public void setCharacter(Entity character) {
        this.character = character;
    }

    public int getThreat() {
        return threat;
    }

    public void setThreat(int threat) {
        this.threat = threat;
    }

    public Pickable getPickable() {
        return pickable;
    }

    public void setPickable(Pickable pickable) {
        this.pickable = pickable;
    }

    public int getOffset() {
        return offset;
    }
}
