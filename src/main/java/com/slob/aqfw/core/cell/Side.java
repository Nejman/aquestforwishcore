package com.slob.aqfw.core.cell;

public enum Side {
    LEFT(0),
    LEFT_DOWN(1),
    RIGHT_DOWN(2),
    RIGHT(3),
    RIGHT_UP(4),
    LEFT_UP(5);

    private final int sideCode;


    Side(int sideCode) {
        this.sideCode = sideCode;
    }

    public int getSideCode() {
        return sideCode;
    }

    public static Side getSideForCode(int code){
        if (code > 5) {
            code = code % 6;
        }
        for(Side side : Side.values()){
            if(side.getSideCode() == code){
                return side;
            }
        }
        throw new IllegalArgumentException("Unknown side: " + code);
    }

    public static Side getOppositeSide(int code){
        code = code + 3;
        code = code % 6;
        return getSideForCode(code);
    }
}
