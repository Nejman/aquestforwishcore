package com.slob.aqfw.core.cell;

public interface Pickable {
    void pickUp();
}
