package com.slob.aqfw.core.cell;

import java.util.*;


/*  Example of generated field, each cell is represented by (xy) coordinates value


                    08  18  28  38  48

                  07  17  27  37  47  57

				06  16  26  36  46  56  66

			  05  15  25  35  45  55  65  75

			04  14  24  34  44  54  64  74  84

			  03  13  23  33  43  53  63  73

				02  12  22  32  42  52  62

				  01  11  21  31  41  51

					00  10  20  30  40
*/


public class HexCreator {
    private static  Map<Integer, List<Cell>> rowMap = new HashMap<>();

    public static List<Cell> createHexField(int size){
        List<Cell> result = new LinkedList<>();
        int rowSize = size;
        int rowId = 0;
        int offset = 0;
        while (rowSize <= (size * 2) - 1){
            result.addAll(createRow(rowSize, rowId, offset));
            rowId++;
            rowSize++;
            offset++;
        }
        offset--;
        offset--;
        rowSize--;
        while (rowSize > size){
            rowSize--;
            result.addAll(createRow(rowSize, rowId, offset));
            rowId++;
            offset--;
        }
        connectAllRows();
        return result;
    }

    private static String getNewHexId(){
        return UUID.randomUUID().toString();
    }

    private static List<Cell> createRow(int size, int rowId, int offset){
        List<Cell> result = new LinkedList<>();
        for(int i = 0; i < size; i++){
            Cell cell = new Cell(i, rowId, getNewHexId(), offset);
            result.add(cell);
        }
        rowMap.put(rowId, result);
        connectRow(result);
        return result;
    }

    private static void connectRow(List<Cell> cells){
        for(Cell cell : cells){
            for (Cell connected : cells){
                if(!cell.equals(connected)
                && !cell.getNeighbours().containsValue(connected)
                && cell.getX() == connected.getX() - 1 ){
                    cell.getNeighbours().put(Side.RIGHT, connected);
                    connected.getNeighbours().put(Side.LEFT, cell);
                }
            }
        }
    }

    private static void connectAllRows(){
        for (int i = 0; i < rowMap.size() - 1; i++){
            List<Cell> lowerList = rowMap.get(i);
            List<Cell> upperList = rowMap.get(i+1);
            if(lowerList.size() < upperList.size()){
                for(Cell cell : lowerList){
                    int lowerX = cell.getX();
                    Cell upperLeft = getCellWithXValue(lowerX, upperList);
                    Cell upperRight = getCellWithXValue(lowerX + 1, upperList);
                    if(upperLeft != null) {
                        cell.getNeighbours().put(Side.LEFT_UP, upperLeft);
                        upperLeft.getNeighbours().put(Side.RIGHT_DOWN, cell);
                    }
                    if(upperRight != null) {
                        cell.getNeighbours().put(Side.RIGHT_UP, upperRight);
                        upperRight.getNeighbours().put(Side.LEFT_DOWN, cell);
                    }
                }
            } else {
                for(Cell cell : upperList){
                    int upperX = cell.getX();
                    Cell lowerLeft = getCellWithXValue(upperX, lowerList);
                    Cell lowerRight = getCellWithXValue(upperX + 1, lowerList);
                    if(lowerLeft != null) {
                        cell.getNeighbours().put(Side.LEFT_DOWN, lowerLeft);
                        lowerLeft.getNeighbours().put(Side.RIGHT_UP, cell);
                    }
                    if(lowerRight != null) {
                        cell.getNeighbours().put(Side.RIGHT_DOWN, lowerRight);
                        lowerRight.getNeighbours().put(Side.LEFT_UP, cell);
                    }
                }
            }
        }
    }

    private static Cell getCellWithXValue(int x, List<Cell> cells){
        for (Cell cell : cells){
            if (cell.getX() == x){
                return cell;
            }
        }
        return null;
    }
}
