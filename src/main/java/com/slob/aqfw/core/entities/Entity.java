package com.slob.aqfw.core.entities;

import com.google.common.collect.ImmutableMap;
import com.slob.aqfw.core.agony.Agony;
import com.slob.aqfw.core.cell.Side;
import com.slob.aqfw.core.effect.Effect;

import java.util.List;

public class Entity {
    protected final String id;
    protected final int defaultMaxHealth;
    protected final ImmutableMap<Side, Integer> defaultArmour;
    protected final List<Effect> currentEffects;
    protected int rotation;
    protected int currentHealth;
    protected List<Agony> agonyList;

    public Entity(String id, int defaultMaxHealth, ImmutableMap<Side, Integer> defaultArmour, List<Effect> currentEffects, int rotation, int currentHealth, List<Agony> agonyList) {
        this.id = id;
        this.defaultMaxHealth = defaultMaxHealth;
        this.defaultArmour = defaultArmour;
        this.currentEffects = currentEffects;
        this.rotation = rotation;
        this.currentHealth = currentHealth;
        this.agonyList = agonyList;
    }

    public List<Effect> getActiveEffects() {
        return currentEffects;
    }

    public void addEffect(Effect effect){
        currentEffects.add(effect);
    }

    public void resetEffects(){
        currentEffects.clear();
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public String getId() {
        return id;
    }

    public int getDefaultMaxHealth() {
        return defaultMaxHealth;
    }

    public int getRotation() {
        return rotation;
    }

    public ImmutableMap<Side, Integer> getDefaultArmourCopy() {
        return ImmutableMap.copyOf(defaultArmour);
    }

    public List<Agony> getAgonyList() {
        return agonyList;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }
}
