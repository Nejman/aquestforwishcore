package com.slob.aqfw.core.cell;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CellCreatorTest {

    @Test
    public void createEmptyHexField() {
        List<Cell> cells = HexCreator.createHexField(0);
        Assert.assertTrue(cells.isEmpty());
    }
    @Test
    public void createHexFieldSizeOne() {
        List<Cell> cells = HexCreator.createHexField(1);
        Assert.assertEquals(1, cells.size());
    }
    @Test
    public void createHexFieldSizeTwo() {
        List<Cell> cells = HexCreator.createHexField(2);
        Assert.assertEquals(7, cells.size());
    }


    @Test
    public void createHexFieldSizeThree() {
        List<Cell> cells = HexCreator.createHexField(3);
        Assert.assertEquals(19, cells.size());
    }

    @Test
    public void createHexFieldSizeFour() {
        List<Cell> cells = HexCreator.createHexField(4);
        Assert.assertEquals(37, cells.size());
    }

    @Test
    public void createHexFieldSizeFive() {
        List<Cell> cells = HexCreator.createHexField(5);
        Assert.assertEquals(61, cells.size());
    }
}