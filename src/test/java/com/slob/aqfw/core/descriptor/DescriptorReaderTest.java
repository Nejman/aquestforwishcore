package com.slob.aqfw.core.descriptor;

import org.junit.Assert;
import org.junit.Test;

public class DescriptorReaderTest {

    @Test
    public void initTest() {
        //given
        DescriptorRepository repository = new DescriptorRepository();
        DescriptorReader reader = new DescriptorReader(repository);

        //when
        GenericDescriptor beastDescriptor = reader.getById(CharacterDescriptor.class, "BEAST_ID");

        //then
        Assert.assertNotNull(beastDescriptor);
        Assert.assertTrue(beastDescriptor instanceof CharacterDescriptor);
        CharacterDescriptor descriptor = (CharacterDescriptor) beastDescriptor;
        Assert.assertEquals("BEAST_ID", beastDescriptor.getId());
        Assert.assertEquals(10, descriptor.getDefaultMaxHealth());
        Assert.assertEquals(3, descriptor.getMaxPoints().size());
        Assert.assertEquals("ATTACK", descriptor.getMaxPoints().get(0).pointType);
        Assert.assertEquals((Integer) 1, descriptor.getMaxPoints().get(0).amount);
        Assert.assertEquals("MOVEMENT", descriptor.getMaxPoints().get(1).pointType);
        Assert.assertEquals((Integer) 1, descriptor.getMaxPoints().get(1).amount);
        Assert.assertEquals("ACTION", descriptor.getMaxPoints().get(2).pointType);
        Assert.assertEquals((Integer) 1, descriptor.getMaxPoints().get(2).amount);
        Assert.assertEquals(2, descriptor.getDefaultArmour().size());
        Assert.assertEquals((Integer) 5, descriptor.getDefaultArmour().get(0).side);
        Assert.assertEquals((Integer) 1, descriptor.getDefaultArmour().get(0).amount);
        Assert.assertEquals((Integer) 1, descriptor.getDefaultArmour().get(1).side);
        Assert.assertEquals((Integer) 1, descriptor.getDefaultArmour().get(1).amount);
        Assert.assertEquals("GOOD", descriptor.getAlignment());
        Assert.assertEquals(4, descriptor.getDefaultSpeed());
        Assert.assertEquals(3, descriptor.getBaseAttackValue());
        Assert.assertEquals(4, descriptor.getThreatLevel());
        Assert.assertEquals(7, descriptor.getSkills().size());
        Assert.assertEquals("BASE_ATTACK", descriptor.getSkills().get(0));
        Assert.assertNull(descriptor.getAgonyList());
    }

}